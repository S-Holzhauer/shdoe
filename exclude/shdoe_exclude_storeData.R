# TODO correct/adopt

	
	source(pix$path_names[["database"]])
	
	data <- list()
	for (h in 1:length(runID$startID)) {
		if (is.null(lix$where)) 			lix$where	 		<<- "true"
		
		if (length(dataNames[dataNames[, infoCol[['figure']]] == 1, infoCol[['name']]]) == 0) {
			throw("Choose data column in dataNames matrix!")
		}
		column <- dataNames[dataNames[, infoCol[['figure']]] == 1, infoCol[['sql']]]
		
		statement = paste(
				"SELECT ",
				column,
				" FROM ",
				dataNames[dataNames[, infoCol[['figure']]] == 1, infoCol[['table']]], " ",
				"WHERE ",
				"runID", ">=" , runConfig$startRunID[h], " AND ",
				"runID", "<=" , runConfig$endRunID[h], " and ",
				lix$where,
				" GROUP BY runID", 
				
				sep=""
		)
		
		print(statement)
		data <- c(data, as.vector(as.matrix(my.getData(statement))))
	}
	
	sh.doe.data <- data
	save(list='sh.doe.data', file=paste(pix$path_names["out_dir_data"],'/exampleData_simulations_numRuns.rda', sep=""))
	