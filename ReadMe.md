shdoe
=============

R package for the conduction of Design of Experiment for ABM. Feature include:

 * Determine number of required runs
 
 * Calculate effect sizes
 
 * Visualisation of required number of runs
 
 * Visualisation of statistics
 
 * Visualisation of interaction effects
 
# Installation

``devtools::install_bitbucket("S-Holzhauer/shdoe@master")``


# First Steps

Start by reading the introduction vignette:  
``browseVignettes("shdoe")``


# Contact

If you're interested in using the package, or for discussions, questions and feature requests don't hesitate to contact
sascha.holzhauer@uni-kassel.de