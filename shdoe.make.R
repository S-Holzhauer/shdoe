library(roxygen2)
library("devtools")
full <- TRUE

# has_devel() 	# does not work because of 'C:\Program' is not recognized as an internal or 
				# external command, operable program or batch file.

pkgsName <- "shdoe"
setwd("~/git/shdoe")

# create(pkgsName)
#devtools::use_vignette("shdoe-intro")

if (full) {
	document()
}

setwd("..")

if (full) {
	devtools::build_vignettes(pkgsName)
}

install(pkgsName, upgrade="always", build_vignettes=T)

#devtools::install_bitbucket("S-Holzhauer/shdoe@default")

#browseVignettes("shdoe")

# devtools::build(pkgsName)

#devtools::build_win(pkg = pkgsName)