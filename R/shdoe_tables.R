#' Write LaTeX table to \code{dexp$dirs$output$tables}.
#' 
#' @param dexp 
#' @param data 
#' @param confidence 
#' @param numruns 
#' @param name 
#' @param label 
#' @return file with LaTeX table
#' 
#' @author Sascha Holzhauer
#' @export
shdoe_table_gammas <- function(dexp, data, confidence = 0.95, numruns = seq(5,20,5), name = NULL,
		label="doe.gamma") {

	gammas <- lapply(numruns, function(numruns) {
				data <- shdoe::shdoe_analyse_gamma4All(dexp = dexp, data = data,
			confidence = confidence, name = name, numruns = numruns)
				do.call(rbind, data)})


	gammas <- do.call(cbind, gammas)
	colnames(gammas) <- numruns
	
	xgammas <- xtable::xtable(gammas,
			label = label,
			caption = sprintf("Relative error for $\\alpha=%.2f$ and %s runs
							for all considered response variables.",
					1 - confidence,
					paste(numruns, collapse="/")))
	
	shbasic::sh.ensurePath(dexp$dirs$output$tables)
	print(xgammas, file=paste(dexp$dirs$output$tables, paste(label,".tex", sep=""), sep = "/"))
}