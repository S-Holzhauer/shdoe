#' Determines the number of required runs from first runConfig definition,
#' statistically.
#' 
#' Either \eqn{data} or a combination of \eqn{runConfig} and \eqn{dataNames}
#' has to be given 
#' 
#' @inheritParams shdoe_analyse_requiredNumberOfRuns
#' @param data sample data (list of one or more data.frames). Columns must be the same for all list elements!
#' @param columns column names of data to calculate statistics for. First column is meant to be \code{runId} 
#' and therefore omitted (unless data consists of only a single column).
#' @return named list or numbers of required runs
#' @seealso \code{\link{shdoe_analyse_requiredNumberOfRuns}} for statistical analysis only
#' @export
shdoe_analyse_requiredNumberOfRuns4All <- function(data, dexp, runconfig, gamma = 0.125, confidence = 0.95,
		name = NULL, printtext= TRUE, columns = colnames(data[[1]])[-1]) {
	
	if (length(colnames(data[[1]])) == 1) {
		columns = colnames(data[[1]])
	}
	
	if (!is.list(data)) {
		R.oo::throw.default("data is not a list!")
	}
	
	results <- list()
	for (r in 1:length(data)) {
		for (column in columns) {
			result <- shdoe_analyse_requiredNumberOfRuns(data[[r]][, column], dexp,
					runconfig = runconfig,
					h = r,
					gamma= gamma,
					confidence = confidence,
					name=paste(name, column, sep=": "),
					logfun = futile.logger::flog.debug
					)
			names(result)  <- column
			results <- append(results, result)
		}
	}
	results
}
#' Statistical way of determining the required number of runs
#' 
#' Text information is printed via futile logger 'shdoe.analyse.runnumber'
#
#' @param data sample data as vector
#' @param dexp Design of Experiment simulation parameter list 
#' @param runconfig used to determine the name of runconfig 
#' @param h numnber of data set in runConfig
#' @param gamma relative error
#' @param confidence confidence interval bounds
#' @param name identifier for the experiment
#' @param ... passed to sd and mean functions
#' @return number of runs when the relative error drops below the
#'    adjusted gamma
#' @export
shdoe_analyse_requiredNumberOfRuns <- function(data,
		dexp,
		runconfig = NULL,
		h = 1,
		gamma = dexp$doe$defaultgamma,	  # originally e
		confidence = dexp$doe$defaultconfidence, # originally b
		loggerdebug = FALSE,
		name="NN",
		logfun = futile.logger::flog.info,
		...) {
	
	if (is.list(data)) {
		futile.logger::flog.error("Argument data must be a vector, not a list!")
	}
	
	confInvHalfL <- shdoe_analyse_ci(data = data,
			dexp = dexp,
			gamma = gamma,
			confidence = confidence)
	
	if (loggerdebug) {
		logfun <- futile.logger::flog.debug
	} else {
		logfun = futile.logger::flog.info
	}
	
	logfun("[%s] %s> Number of required replications with rel. error %f and confidence interval bound %f: %s",
			name,
			if(!is.null(runconfig)) runconfig$name[h] else "",
			gamma,
 			confidence,
			if(is.infinite(confInvHalfL[['n']])) "Inf" else  as.character(as.integer(confInvHalfL[['n']])),
			name="shdoe.analyse.runnumber")

	confInvHalfL[['n']]
}
#' Calculate the running confidence interval half-length.
#' Adapted from \eqn{heR.Misc::run.ci}: Introducing \eqn{xmin} and \eqn{xmax}
#' 
#' Copied from heR.Misc:
#' Calculate the running confidence interval half-length of the sample mean
#' starting with just the first value (NA) and ending with all the values.
#' All returned vectors/matrices have a length/nrows one less than for `x'.
#'
#' Note: Variance might be low for few runs but might raise again for larger numbers of runs.
#' Therefore, it is a good idea to define a minimum number of runs to consider.
#' 
#' @param data sample data as vector
#' @param dexp Design of Experiment simulation parameter list
#' @param gamma relative error
#' @param confidence confidence interval bounds
#' @param ... passed to sd and mean functions
#' @return List with four elements:
#' \item{ci}{vector of confidence interval half-lengths,} 
#' \item{m}{matrix containing columns for X-ci, X, and X+ci,
#' 	  where X is the sample mean, and ci is the confidence half length,}
#' \item{rel}{vector of the relative errors calculated as the confidence interval
#'    half length divided by the sample mean,}
#' \item{n}{The element of x where the relative error drops below the
#'    adjusted`e', which is calculated as e/(1-e).}
#' @seealso \cite{\link{heR.Misc::run.ci}}
#' @references Law, A. M. Simulation Modeling and Analysis McGraw-Hill Professional, 2007
#' @export
shdoe_analyse_ci <- function (data,
		dexp,
		gamma = dexp$doe$defaultgamma,	  # originally e
		confidence = dexp$doe$defaultconfidence,  # originally b
		...
	) {

	if (is.list(data)) {
		futile.logger::flog.error("Argument data must be a vector, not a list!")
	}
	
	alpha <- 1 - confidence
	z <- abs(qnorm(alpha/2))
	
	cint <- NA
	smean <- NA
	
	for (i in 2:length(data)) {
		cint[i - 1] <- z * sd(data[1:i], ...)/sqrt(i)
		smean[i - 1] <- mean(data[1:i], ...)
	}
	rel <- cint / smean
	# to account for when smean == 0 (then, when cint == 0 it's save to set rel = 0)
	rel[cint == 0] <- 0
	
	futile.logger::flog.trace("cint/meant: %s",
			paste(rel, collapse = " / "),
			name = "shdoe.analyse.runnumber")
	
	indices = which(rel <= gamma/(1 - gamma)) + 1
	n <- min(indices[indices >= dexp$doe$yrepmin], Inf)
	n <- replace(n, which(n == Inf), dexp$doe$yrepmax)
	list(ci = cint, m = cbind(smean + cint, as.numeric(smean), 
					smean - cint), rel = rel, n = n)
}
#' Calculate and Draw figures to visualise effect sizes
#' - Design of Experiment (DoE) data
#' @param data response data (vector with same length as paramdata)
#' @param dexp DoE settings object
#' @param paramdata parameter data (factor matrix - should not contain any runid or paramID columns)
#' @param replications number of replications contained in data
#' @param id shown in plot title
#' @param confidence confidence interval to calculate errors
#' @param ... passed to plot function
#' @return plot
#' 
#' @author Sascha Holzhauer
#' @export
shdoe_analyse_effectSizes <- function(data, dexp, paramdata, replications = 2, id = NULL, 
		confidence= 0.975, useplot = FALSE, ...) {
	
	if (dexp$debug$doe > 1) {
		cat("In shdoe_analyse_effectSizes:\n")
		print(head(paramdata))
	}
	
	if(any(!apply(paramdata, 2, is.numeric))) {
		R.oo::throw.default("Parameter data must be numeric for analysis!")
	}

	if(length(paramdata[,1]) != length(data)) {
		R.oo::throw.default(sprintf("Parameter data (%d) and data (%d) must be of same length!",
						length(paramdata[,1]),
						length(data)))
	}
	
	# exclude non-varying parameters:
	variations <- apply(paramdata, 2, function(x) length(unique(x)))
	paramdata <- paramdata[, variations > 1]
	
	# 'factorize' parameter data:
	paramdata <- data.frame(apply(paramdata, 2, cut, breaks=2, c(-1,1)))
	paramdata <- apply(paramdata, 2, as.numeric)
	
	## clean column names:
	colnames(paramdata) <- gsub("[A-Za-z0-9.]*:", "", colnames(paramdata))
	
	## remove runid, paramID if existing
	cnames <- colnames(paramdata)[! colnames(paramdata) %in% c("runid", "paramID")]
	paramdata <- as.data.frame(paramdata[,cnames])
	# if paramdata has only one column..:
	colnames(paramdata) <- cnames
	
	
	### Alternative way with replications from dae:
	# mp <- c("-", "+")
	# fnames <- list(Catal = mp, Temp = mp, Press = mp, Conc = mp)
	# Fac4Proc.Treats <- fac.gen(generate = fnames, each=3, order="standard")
	
	## generate full factorial matrix
	#k <- length(paramDat[1,])
	#full.X <- ffFullMatrix(paramDat, x=seq(1,k), 2)
	
	## create formula
	
	
	fmla <- as.formula(paste("data ~ (", paste(colnames(paramdata)[
			! colnames(paramdata) %in% intersect(names(data), colnames(paramdata))], collapse= " + ", sep=""),")^2"))

	# don't know why needed... - requires a data.frame for data:
	#datas <- merge(paramdata, data, by = intersect(names(data), colnames(paramdata)))
	#datas <- datas[,!colnames(datas) %in% intersect(colnames(data), colnames(paramdata))]
	
	datas <- data.frame(paramdata, data)
	
	
	## calculate anova
	dat.aov <- aov(fmla, datas)
	
	# extract, round, and strip residuals (last entry):
	pvalues <- round(summary(dat.aov)[[1]][["Pr(>F)"]],3)
	pvalues <- pvalues[1:(length(pvalues)-1)]
	
	if (dexp$debug$doe > 0) {
		print(summary(dat.aov))
	}
	
	## determine yates effects	
	effects <- dae::yates.effects(dat.aov, data=datas)

	## calculate confidence intervals:
	### determine variance of replications for each effect:
	if (replications > 1) {
		effectPart <- data.frame(t(effects))[FALSE,]
		for (i in 1:replications) {
			dataPart <- datas[seq(i, nrow(datas), replications ),]
			# TODO does not return all effects!
			effectPart <- rbind(effectPart, t(dae::yates.effects(dat.aov, data=dataPart)))
		}
		
		effectVars <- apply(as.matrix(effectPart), 2, sd)
		errors <- qt(confidence, df= replications-1) * effectVars / sqrt(replications)
		
		effectLower <- effects - errors
		effectUpper <- effects + errors
		
		if (dexp$debug$doe > 0) {
			print(effectVars)
		}
	}
	
	if (length(errors) != length(effects)) {
		R.oo::throw.default("Length for effects and error differ. Check if paramdata and data are ordered correctly 
				and grouped regarding replications!")
	}
	
	###############################
	#### Drawing figures        ###
	###############################

	if (useplot) {
		
		if (dexp$debug$doe > 0) {
			print("Plot now...")
		}
		
		par(mai = c(2.5, 0.5, 0.5, 0.5) * 1)
		plot(0,0
				# boxplot requires a range 1...n
				,xlim=c(1,length(effects))
				,ylim=if (replications > 1) c(min(effectLower), max(effectUpper)) else c(min(effects), max(effects)) 
				,main = paste(dexp$sub$subvaluesfunc("Effect Sizes"), " for ", id, sep =""),
				,xlab = ""
				,ylab = dexp$sub$subvaluesfunc("Effect size")
				,type="n"
				,xaxt="n"
				,cex = 1
				,...
		)
		fields::yline(0)
		fields::xline(8.5, lty=3)
		axis(1, at = 1:length(effects), labels=paste(names(effects), 
						shdoe_analyse_starpvalues(pvalues, starsOnly=TRUE)),
				las=2, 
				cex.axis = 0.5,
				tick=FALSE)
		
		if (replications > 1) {	
			colors <- dexp$colours$genericFun(3, set="Topo")
			points(effectLower
					,col=colors[2]
					,lwd=4)
			
			points(effectUpper
					,col=colors[3]
					,lwd=4)
		 } else {
			colors <- dexp$colours$genericFun(1, set="Topo")
		 }
		
		
		points(effects
				,col=colors[1]
				,lwd=4)
		
		if (replications > 1) {
			legend("topright", c("effect", "lower", "upper"),
					yjust = 0,
					col = colors,
					pch=10,
					cex=2,
					pt.cex = 1,
					bty = "n",
					# move legende:
					#inset = c(-0.08,-0.13)
			)
		}
	}
	return(data.frame(effects = effects, errors = errors, pvalues = pvalues))
}
#' Add stars to the p-values (significance): p < 0.05: *, p < 0.01: **, p < 0.001: ***   
#' @param p p-values
#' @param starsOnly if TRUE, only stars are returned and acutally values omitted
#' @return 'stared' p-values
#' 
#' @author Sascha Holzhauer
#' @export
shdoe_analyse_starpvalues <- function(p, starsOnly = FALSE) {
	if (inherits(p, c("matrix", "data.frame")) && length(dim(p)) == 
			2) {
		apply(p, c(1, 2), shdoe_analyse_starpvalues, starsOnly)
	}
	else {
		if (length(p) > 1) {
			sapply(p, shdoe_analyse_starpvalues, starsOnly)
		}
		else {
			
			s <- ifelse(p > 0.05, "", ifelse(p > 0.01, "*", 
							ifelse(p > 0.001, "**", "***")))
			
			if (is.numeric(p)) { 
				x <- round(p, 3)
				x <- sub("^(-?)0.", "\\1.", sprintf("%.3f", x))
			}
			if (starsOnly) s else paste0(x, s)
		}
	}
}
#' Calculate the relative error for a given interval half-length and number of performed runs.
#' 
#' @param data sample data as vector
#' @param dexp Design of Experiment simulation parameter list
#' @param numruns number of runs to calculate relative error for (\code{< length(data)})
#' @param confidence confidence interval bounds
#' @param ... passed to sd and mean functions
#' @return List with four elements:
#' \item{ci}{vector of confidence interval half-lengths,} 
#' \item{m}{matrix containing columns for X-ci, X, and X+ci,
#' 	  where X is the sample mean, and ci is the confidence half length,}
#' \item{rel}{relative error (calculated as the confidence interval
#'    half length divided by the sample mean) for the number of runs specified}
#' \item{numruns}{given number of runs.}
#' @seealso \cite{\link{shdoe::shdoe_analyse_ci}}
#' @references Law, A. M. Simulation Modeling and Analysis McGraw-Hill Professional, 2007
#' @export
shdoe_analyse_gamma <- function (data,
		dexp,
		numruns,
		confidence = dexp$doe$defaultconfidence,  # originally b
		logfun = futile.logger::flog.info,
		name = "NN",
		...) {
	
	if (is.list(data)) {
		futile.logger::flog.error("Argument data must be a vector, not a list!")
	}
	
	alpha <- 1 - confidence
	z <- abs(qnorm(alpha/2))
	
	cint <- NA
	smean <- NA
	
	for (i in 2:length(data)) {
		cint[i - 1] <- z * sd(data[1:i], ...)/sqrt(i)
		smean[i - 1] <- mean(data[1:i], ...)
	}
	rel <- cint / smean
	# rel = gamma/(1 - gamma)
	# gamma = rel - rel * gamma
	# gamma + gamma * rel = rel
	# gamma (1 + rel) = rel
	# gamma = rel / (1 + rel)
	gamma <- rel[numruns - 1] / (1 + rel[numruns - 1])
	
	logfun("%s> Gamma (rel. error) with number of runs = %d and confidence interval bound %f: %f",
			name,
			as.integer(numruns),
			confidence,
			rel,
			name="shdoe.analyse.runnumber")
	
	list(ci = cint, m = cbind(smean + cint, as.numeric(smean), 
					smean - cint), gamma = gamma, n = numruns)
}
#' Calculate the relative error for a given interval half-length and number of performed runs.
#' 
#' @param data list of data.frame with response variables as columns
#' @param dexp Design of Experiment simulation parameter list
#' @param numruns number of runs to calculate relative error for (\code{< length(data)})
#' @param confidence confidence interval bounds
#' @param ... passed to sd and mean functions
#' @return list of named elements
#' 
#' @author Sascha Holzhauer
#' @export
shdoe_analyse_gamma4All <- function (data,
		dexp,
		numruns,
		confidence = dexp$doe$defaultconfidence,  # originally b
		columns = colnames(data[[1]])[-1],
		name = "Gamma",
		...) {
	
	if (!is.list(data)) {
		R.oo::throw.default("data is not a list!")
	}
	
	results <- list()
	for (r in 1:length(data)) {
		for (column in columns) {
			result <- shdoe_analyse_gamma(data[[r]][, column], dexp,
					numruns = numruns,
					confidence = confidence,
					name=paste(name, column, sep=": ")
			)$gamma
			names(result)  <- column
			results <- append(results, result)
		}
	}
	
	results
}