###############################################################################
# Calculate and Draw figures:
# - Design of Experiment (DoE) data
#
# Model:		Sonomode
# Purpose:		Visualise effect sizes
#
# Be aware of: 	-
#
# Base-Script:	-
# Version: 		1.00 - 02.11.2011
#
# Author: 		Friedrich Krebs, CESR
#				Sascha Holzhauer, CESR
###############################################################################

my.ts.doe.effectSizes <- function(data, figureInfo, runConfig, paramData, confidence= 0.975, ...) {
	
	library(dae)
	library(fields)
	
	source(pix$path_names[["figUtils"]])
	source(pix$path_names[["substitute"]])
	source(pix$path_names[["colors"]])
	
	## test data
	#data <- data.frame(data = c(1, 1, 0.8, 1, 2, 3, 2, 2))
	#paramData <- data.frame(PROP_EXTREMISTS = c(0.01, 0.01, 0.01, 0.01, 0.2, 0.2, 0.2, 0.2), MUE = c(0, 0, 1,  1, 0, 0, 1, 1))
	
	
	## generate factorial matrix:
	#factor(c(1,2,1,2))
	#factor(paramData[,4])
	colnames(paramData)
	paramData <- my.substituteValues(paramData)
	
	as.numeric(as.matrix(paramData[1, -match(c("runID", "id"), names(paramData))]))
	# summary(paramData)
	paramDat <- apply(paramData[, -match(c("runID", "id"), names(paramData))], 2, cut, breaks=2, c(-1,1))
	paramDat <- apply(paramDat, 2, as.numeric)
	
	#dat.dat <- data$data
	data <- data[, figureInfo[figureInfo[, infoCol[['type']]] == 5, infoCol[['name']]]]
	
	### Alternative way with replications from dae:
	# mp <- c("-", "+")
	# fnames <- list(Catal = mp, Temp = mp, Press = mp, Conc = mp)
	# Fac4Proc.Treats <- fac.gen(generate = fnames, each=3, order="standard")
	
	## generate full factorial matrix
	#k <- length(paramDat[1,])
	#full.X <- ffFullMatrix(paramDat, x=seq(1,k), 2)
	
	## create formula
	
	colnames(paramDat) <- gsub("[A-Za-z0-9.]*:", "", colnames(paramDat)) 
	(fmla <- as.formula(paste("data ~ (", paste(colnames(paramDat), collapse= "+", sep=""),")^2")))
	daten <- data.frame(paramDat, data)
	
	## calculate anova
	dat.aov <- aov(fmla, daten)
	
	print(summary(dat.aov))
	
	## determine yates effects	
	effects <- round(yates.effects(dat.aov, data=daten), 2)
	
	
	## calculate confidence intervals:
	### determine variance of replications for each effect:
	rep <- runConfig$average[1]
	#rep <- 2
	effectPart <- data.frame(t(effects))[FALSE,]
	for (i in 1:rep) {
		dataPart <- daten[seq(i, length(daten[,1]), rep ),]
		effectPart <- rbind(effectPart, t(yates.effects(dat.aov, data=dataPart)))
	}
	
	effectVars <- apply(as.matrix(effectPart), 2, sd)
	errors <- qt(0.975, df= rep-1) * effectVars / sqrt(rep)
	
	effectLower <- effects - errors
	effectUpper <- effects + errors
	
	print(effectVars)
	
	###############################
	#### Parameter Data         ###
	###############################
	
	runIDs <- my.runIdRangeFromRunConfig(runConfig)
	
	###############################
	#### Drawing figures        ###
	###############################
	
	print("Plot now...")
	
	par(mai = c(2.5, 0.5, 0.5, 0.5) * 1)
	plot(0,0
		# boxplot requires a ramge 1...n
		,xlim=c(1,length(effects))
		,ylim=c(min(effectLower), max(effectUpper))
		,main = paste(lix$titlePre, paste(": ", my.substitute("Effect Sizes"), " for runIDs ", my.makeCondensedIdSet(runIDs), sep =""), 
				" to ", my.subLabel(substituteLabels, figureInfo[figureInfo[, infoCol[['type']]] == 5, infoCol[['name']]]), " ", lix$titleAdd, sep="")

		,xlab = ""
		#,xlab = my.subLabel(substituteLabels, 'Parameter')
		,ylab = my.subLabel(substituteLabels, "Effect size")
		,type="n"
		,xaxt="n"
		,cex = 1
		#,yaxt="n"
		#,xaxp = c(runConfig$startRunID[1], runConfig$endRunID[1], runConfig$average[1])
		,...
		)
	yline(0)
	xline(8.5, lty=3)
	axis(1, at = 1:length(effects), labels=names(effects),las=3, cex.axis = 0.5, tick=FALSE)
	colors <- my.getColorSet(3, set="Topo")


	
	points(effectLower
			,col=colors[2]
			,lwd=4)
	
	points(effectUpper
			,col=colors[3]
			,lwd=4)
	
	points(effects
			,col=colors[1]
			,lwd=4)
	
	legend("topright",
			c("effect", "lower", "upper" ),
			yjust = 0,
			col = colors,
			pch=10,
			cex=2,
			pt.cex = 1,
			#lty = lineStyle,
			# Abstand zwischen Symbolen und Text:
			#x.intersp = 0.25,
			# Abstand zwischen den Zeilen
			#y.intersp = 0.2,
			bty = "n",
	# "Verschieben" der Legende vom Rand:
	#inset = c(-0.08,-0.13)
	)
}

